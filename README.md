# Project Management

## Functionalities

### User Module

- User registration
- User login
- Forgot Password
- Update Password

### Task Management Module

- Create Project
- Add tasks to the project
- add task timeline
- Add multiple assignees and collaborators to the task
- Set task priority

# Setting Up Frappe on Ubuntu 22.04

In this guide, we'll walk through the steps to configure a fresh installation of Ubuntu 22.04 to set up an environment and install Frappe version 14 and project
management app. Let's get started!

## Prerequisites

Before we begin, ensure you have the following prerequisites in place for optimal functionality of ERPNext on your server.

### Software Requirements

- Updated Ubuntu 22.04
- A user with sudo privileges
- Python 3.10+
- Node.js 16

### Hardware Requirements

- 4GB RAM
- 40GB Hard Disk

## Server Settings

### Update and Upgrade Packages

```bash
sudo apt-get update -y
sudo apt-get upgrade -y
```

### Create a New User (Bench User)

```bash
sudo adduser [frappe-user]
usermod -aG sudo [frappe-user]
su [frappe-user]
cd /home/[frappe-user]
```

Replace `[frappe-user]` with your desired username.

### Install Required Packages

#### Install GIT

```bash
sudo apt-get install git
```

#### Install Python

```bash
sudo apt-get install python3-dev python3.10-dev python3-setuptools python3-pip python3-distutils
```

#### Install Python Virtual Environment

```bash
sudo apt-get install python3.10-venv
```

#### Install Software Properties Common

```bash
sudo apt-get install software-properties-common
```

#### Install MariaDB

```bash
sudo apt install mariadb-server mariadb-client
```

#### Install Redis Server

```bash
sudo apt-get install redis-server
```

#### Install Other Packages

```bash
sudo apt-get install xvfb libfontconfig wkhtmltopdf
sudo apt-get install libmysqlclient-dev
```

### Configure MYSQL Server

#### Setup the Server

```bash
sudo mysql_secure_installation
```

Follow the prompts to complete the setup.

#### Edit MYSQL Default Config File

```bash
sudo nano /etc/mysql/my.cnf
```

Add the following block of code and save:

```
[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci

[mysql]
default-character-set = utf8mb4
```

#### Restart the MYSQL Server

```bash
sudo service mysql restart
```

### Install CURL, Node, NPM and Yarn

#### Install CURL

```bash
sudo apt install curl
```

#### Install Node

```bash
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.profile
nvm install 16.15.0
```

#### Install NPM

```bash
sudo apt-get install npm
```

#### Install Yarn

```bash
sudo npm install -g yarn
```

### Install Frappe Bench

```bash
sudo pip3 install frappe-bench
```

### Initialize Frappe Bench

```bash
bench init --frappe-branch version-14 frappe-bench
```

### Change User Directory Permissions

```bash
chmod -R o+rx /home/[frappe-user]
```

### Enter bench directory

```bash
cd frappe-bench
```

### Create a New Site

```bash
bench new-site [site-name]
```

Replace `[site-name]` with your desired site name.

### Get the `project_management` app

```bash
bench get-app --branch main project_management https://gitlab.com/divin167/qt-assessment.git
```

### Install the `project_management` app on your Site

```bash
bench --site [site-name] install-app project_management
```

### Set your Site as default

```bash
bench use [site-name]
```

### Start the application

```bash
bench start
```

For more installation info refer to <https://codewithkarani.com/2022/08/18/install-erpnext-version-14>
