// Copyright (c) 2024, Divin Fiston ISHIMWE and contributors
// For license information, please see license.txt

frappe.ui.form.on('Task', {
   refresh: function (frm) {
      frm.set_query('project', () => {
         return {
            filters: {
               docstatus: 1,
            },
         };
      });
      document.querySelector(`input[id='${frm.doc.priority.toLowerCase()}']`).setAttribute('checked', 1);
      if (frm.doc.docstatus == 1) {
         document.querySelector("input[id='low']").setAttribute('disabled', 1);
         document.querySelector("input[id='normal']").setAttribute('disabled', 1);
         document.querySelector("input[id='high']").setAttribute('disabled', 1);
      }
   },
   onload_post_render: function (frm) {
      if (frm.doc.docstatus === 1) document.querySelector("input[name='priority']").setAttribute('disabled', 1);
      window.addEventListener('change', function (e) {
         let changed_element = e.target;
         if (changed_element.getAttribute('name').includes('priority')) {
            frm.set_value('priority', changed_element.value);
         }
      });
   },
   end_date: function (frm) {
      frm.events.validate_date_range(frm);
   },
   description: function (frm) {
      if (frm.doc.description.length > 0 && frm.doc.description.length <= 30) frm.events.set_css(frm, "[data-fieldname='description']", '1px green solid');
      else if (frm.doc.description.length > 30) frm.events.set_css(frm, "[data-fieldname='description']", '1px red solid');
      else frm.events.set_css(frm, "[data-fieldname='description']", '1px f4f5f6 solid');
   },
   validate: function (frm) {
      frm.set_value('priority', document.querySelector("input[name='priority']:checked").value);
      frm.events.validate_date_range(frm);
   },
   set_css: function (frm, field, value) {
      document.querySelectorAll(field)[1].style.border = value;
   },
   validate_date_range: function (frm) {
      if (frm.doc.start_date > frm.doc.end_date) {
         frappe.throw('End date should be greater that start date');
      }
   },
});
